package name.berger.max.mil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

import org.apache.commons.exec.ExecuteStreamHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Execution Hander collecting stdout as string.
 * 
 * @version $Date$
 */
public class StringExecuteStreamHandler implements ExecuteStreamHandler,
        Runnable {

    private static final Log LOGGER = LogFactory
            .getLog(StringExecuteStreamHandler.class);

    // StringBuffer is thread safe.
    private final StringBuffer s = new StringBuffer();
    private InputStream is;
    private volatile boolean done;

    /**
     * Default constructor.
     */
    public StringExecuteStreamHandler() {
        // nothing to do.
    }

    /** {@inheritDoc} */
    public void setProcessErrorStream(final InputStream inputstream)
            throws IOException {
        // ignore error stream
    }

    /** {@inheritDoc} */
    public void setProcessInputStream(final OutputStream outputstream)
            throws IOException {
        // no input stream
    }

    /** {@inheritDoc} */
    public void setProcessOutputStream(final InputStream inputstream)
            throws IOException {
        this.is = inputstream;
    }

    /** {@inheritDoc} */
    public void start() throws IOException {
        new Thread(this).start();
    }

    /** {@inheritDoc} */
    public void stop() {
        this.done = true;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return this.s.toString();
    }

    /** {@inheritDoc} */
    public void run() {
        final BufferedReader r = new BufferedReader(new InputStreamReader(
                this.is));
        try {
            String l = r.readLine();
            while ((l != null) && !this.done) {
                this.s.append(l);
                l = r.readLine();
            }
        } catch (final IOException e) {
            StringExecuteStreamHandler.LOGGER.debug(
                    "Error reading from external process", e);
        }
    }

}
