package name.berger.max.mil;

import java.io.IOException;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Tries to detect which theme is set in the current Gnome settings.
 * 
 * @version $Date$
 */
public class GnomeThemeDetector {

    private static final Log LOGGER = LogFactory
            .getLog(GnomeThemeDetector.class);

    /**
     * Default constructor.
     */
    public GnomeThemeDetector() {
        // Nothing to do.
    }

    /**
     * Actually detect the GNOME theme.
     * 
     * @return Name of the theme or null if any error occurred.
     */
    public String getTheme() {
        final DefaultExecutor exec = new DefaultExecutor();
        final StringExecuteStreamHandler sh = new StringExecuteStreamHandler();
        exec.setStreamHandler(sh);
        final CommandLine cl = new CommandLine("gconftool-2");
        cl.addArguments(new String[] { "--get",
                "/desktop/gnome/interface/icon_theme", });
        try {
            final int exitvalue = exec.execute(cl);
            if (exitvalue == 0) {
                return sh.toString();
            }
        } catch (final ExecuteException e) {
            GnomeThemeDetector.LOGGER.debug(e.toString(), e);
        } catch (final IOException e) {
            GnomeThemeDetector.LOGGER.debug(e.toString(), e);
        }
        return null;
    }

}
