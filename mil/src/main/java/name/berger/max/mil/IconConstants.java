/**
 * 
 */
package name.berger.max.mil;

/**
 * This class defines constants for all default icons. Each icon given here is
 * part of the tango icon set, and is therefore guaranteed to be available.
 * 
 * 
 * @version $Date: 1/1/2000$
 */
public final class IconConstants {

    /** Constant for group actions. */
    public static final String GROUP_ACTIONS = "actions";
    /** Constant for group animations. */
    public static final String GROUP_ANIMATIONS = "animations";
    /** Constant for group apps. */
    public static final String GROUP_APPS = "apps";
    /** Constant for group categories. */
    public static final String GROUP_CATEGORIES = "categories";
    /** Constant for group devices. */
    public static final String GROUP_DEVICES = "devices";
    /** Constant for group emblems. */
    public static final String GROUP_EMBLEMS = "emblems";
    /** Constant for group emotes. */
    public static final String GROUP_EMOTES = "emotes";
    /** Constant for group mimetypes. */
    public static final String GROUP_MIMETYPES = "mimetypes";
    /** Constant for group places. */
    public static final String GROUP_PLACES = "places";
    /** Constant for group status. */
    public static final String GROUP_STATUS = "status";

    /** Constant for icon actions/address-book-new. */
    public static final String ACTIONS_ADDRESS_BOOK_NEW = "actions/address-book-new";
    /** Constant for icon actions/appointment-new. */
    public static final String ACTIONS_APPOINTMENT_NEW = "actions/appointment-new";
    /** Constant for icon actions/bookmark-new. */
    public static final String ACTIONS_BOOKMARK_NEW = "actions/bookmark-new";
    /** Constant for icon actions/contact-new. */
    public static final String ACTIONS_CONTACT_NEW = "actions/contact-new";
    /** Constant for icon actions/document-new. */
    public static final String ACTIONS_DOCUMENT_NEW = "actions/document-new";
    /** Constant for icon actions/document-open. */
    public static final String ACTIONS_DOCUMENT_OPEN = "actions/document-open";
    /** Constant for icon actions/document-print. */
    public static final String ACTIONS_DOCUMENT_PRINT = "actions/document-print";
    /** Constant for icon actions/document-print-preview. */
    public static final String ACTIONS_DOCUMENT_PRINT_PREVIEW = "actions/document-print-preview";
    /** Constant for icon actions/document-properties. */
    public static final String ACTIONS_DOCUMENT_PROPERTIES = "actions/document-properties";
    /** Constant for icon actions/document-save. */
    public static final String ACTIONS_DOCUMENT_SAVE = "actions/document-save";
    /** Constant for icon actions/document-save-as. */
    public static final String ACTIONS_DOCUMENT_SAVE_AS = "actions/document-save-as";
    /** Constant for icon actions/edit-clear. */
    public static final String ACTIONS_EDIT_CLEAR = "actions/edit-clear";
    /** Constant for icon actions/edit-copy. */
    public static final String ACTIONS_EDIT_COPY = "actions/edit-copy";
    /** Constant for icon actions/edit-cut. */
    public static final String ACTIONS_EDIT_CUT = "actions/edit-cut";
    /** Constant for icon actions/edit-delete. */
    public static final String ACTIONS_EDIT_DELETE = "actions/edit-delete";
    /** Constant for icon actions/edit-find. */
    public static final String ACTIONS_EDIT_FIND = "actions/edit-find";
    /** Constant for icon actions/edit-find-replace. */
    public static final String ACTIONS_EDIT_FIND_REPLACE = "actions/edit-find-replace";
    /** Constant for icon actions/edit-paste. */
    public static final String ACTIONS_EDIT_PASTE = "actions/edit-paste";
    /** Constant for icon actions/edit-redo. */
    public static final String ACTIONS_EDIT_REDO = "actions/edit-redo";
    /** Constant for icon actions/edit-select-all. */
    public static final String ACTIONS_EDIT_SELECT_ALL = "actions/edit-select-all";
    /** Constant for icon actions/edit-undo. */
    public static final String ACTIONS_EDIT_UNDO = "actions/edit-undo";
    /** Constant for icon actions/folder-new. */
    public static final String ACTIONS_FOLDER_NEW = "actions/folder-new";
    /** Constant for icon actions/format-indent-less. */
    public static final String ACTIONS_FORMAT_INDENT_LESS = "actions/format-indent-less";
    /** Constant for icon actions/format-indent-more. */
    public static final String ACTIONS_FORMAT_INDENT_MORE = "actions/format-indent-more";
    /** Constant for icon actions/format-justify-center. */
    public static final String ACTIONS_FORMAT_JUSTIFY_CENTER = "actions/format-justify-center";
    /** Constant for icon actions/format-justify-fill. */
    public static final String ACTIONS_FORMAT_JUSTIFY_FILL = "actions/format-justify-fill";
    /** Constant for icon actions/format-justify-left. */
    public static final String ACTIONS_FORMAT_JUSTIFY_LEFT = "actions/format-justify-left";
    /** Constant for icon actions/format-justify-right. */
    public static final String ACTIONS_FORMAT_JUSTIFY_RIGHT = "actions/format-justify-right";
    /** Constant for icon actions/format-text-bold. */
    public static final String ACTIONS_FORMAT_TEXT_BOLD = "actions/format-text-bold";
    /** Constant for icon actions/format-text-italic. */
    public static final String ACTIONS_FORMAT_TEXT_ITALIC = "actions/format-text-italic";
    /** Constant for icon actions/format-text-strikethrough. */
    public static final String ACTIONS_FORMAT_TEXT_STRIKETHROUGH = "actions/format-text-strikethrough";
    /** Constant for icon actions/format-text-underline. */
    public static final String ACTIONS_FORMAT_TEXT_UNDERLINE = "actions/format-text-underline";
    /** Constant for icon actions/go-bottom. */
    public static final String ACTIONS_GO_BOTTOM = "actions/go-bottom";
    /** Constant for icon actions/go-down. */
    public static final String ACTIONS_GO_DOWN = "actions/go-down";
    /** Constant for icon actions/go-first. */
    public static final String ACTIONS_GO_FIRST = "actions/go-first";
    /** Constant for icon actions/go-home. */
    public static final String ACTIONS_GO_HOME = "actions/go-home";
    /** Constant for icon actions/go-jump. */
    public static final String ACTIONS_GO_JUMP = "actions/go-jump";
    /** Constant for icon actions/go-last. */
    public static final String ACTIONS_GO_LAST = "actions/go-last";
    /** Constant for icon actions/go-next. */
    public static final String ACTIONS_GO_NEXT = "actions/go-next";
    /** Constant for icon actions/go-previous. */
    public static final String ACTIONS_GO_PREVIOUS = "actions/go-previous";
    /** Constant for icon actions/go-top. */
    public static final String ACTIONS_GO_TOP = "actions/go-top";
    /** Constant for icon actions/go-up. */
    public static final String ACTIONS_GO_UP = "actions/go-up";
    /** Constant for icon actions/list-add. */
    public static final String ACTIONS_LIST_ADD = "actions/list-add";
    /** Constant for icon actions/list-remove. */
    public static final String ACTIONS_LIST_REMOVE = "actions/list-remove";
    /** Constant for icon actions/mail-forward. */
    public static final String ACTIONS_MAIL_FORWARD = "actions/mail-forward";
    /** Constant for icon actions/mail-mark-junk. */
    public static final String ACTIONS_MAIL_MARK_JUNK = "actions/mail-mark-junk";
    /** Constant for icon actions/mail-mark-not-junk. */
    public static final String ACTIONS_MAIL_MARK_NOT_JUNK = "actions/mail-mark-not-junk";
    /** Constant for icon actions/mail-message-new. */
    public static final String ACTIONS_MAIL_MESSAGE_NEW = "actions/mail-message-new";
    /** Constant for icon actions/mail-reply-all. */
    public static final String ACTIONS_MAIL_REPLY_ALL = "actions/mail-reply-all";
    /** Constant for icon actions/mail-reply-sender. */
    public static final String ACTIONS_MAIL_REPLY_SENDER = "actions/mail-reply-sender";
    /** Constant for icon actions/mail-send-receive. */
    public static final String ACTIONS_MAIL_SEND_RECEIVE = "actions/mail-send-receive";
    /** Constant for icon actions/media-eject. */
    public static final String ACTIONS_MEDIA_EJECT = "actions/media-eject";
    /** Constant for icon actions/media-playback-pause. */
    public static final String ACTIONS_MEDIA_PLAYBACK_PAUSE = "actions/media-playback-pause";
    /** Constant for icon actions/media-playback-start. */
    public static final String ACTIONS_MEDIA_PLAYBACK_START = "actions/media-playback-start";
    /** Constant for icon actions/media-playback-stop. */
    public static final String ACTIONS_MEDIA_PLAYBACK_STOP = "actions/media-playback-stop";
    /** Constant for icon actions/media-record. */
    public static final String ACTIONS_MEDIA_RECORD = "actions/media-record";
    /** Constant for icon actions/media-seek-backward. */
    public static final String ACTIONS_MEDIA_SEEK_BACKWARD = "actions/media-seek-backward";
    /** Constant for icon actions/media-seek-forward. */
    public static final String ACTIONS_MEDIA_SEEK_FORWARD = "actions/media-seek-forward";
    /** Constant for icon actions/media-skip-backward. */
    public static final String ACTIONS_MEDIA_SKIP_BACKWARD = "actions/media-skip-backward";
    /** Constant for icon actions/media-skip-forward. */
    public static final String ACTIONS_MEDIA_SKIP_FORWARD = "actions/media-skip-forward";
    /** Constant for icon actions/process-stop. */
    public static final String ACTIONS_PROCESS_STOP = "actions/process-stop";
    /** Constant for icon actions/system-lock-screen. */
    public static final String ACTIONS_SYSTEM_LOCK_SCREEN = "actions/system-lock-screen";
    /** Constant for icon actions/system-log-out. */
    public static final String ACTIONS_SYSTEM_LOG_OUT = "actions/system-log-out";
    /** Constant for icon actions/system-search. */
    public static final String ACTIONS_SYSTEM_SEARCH = "actions/system-search";
    /** Constant for icon actions/system-shutdown. */
    public static final String ACTIONS_SYSTEM_SHUTDOWN = "actions/system-shutdown";
    /** Constant for icon actions/tab-new. */
    public static final String ACTIONS_TAB_NEW = "actions/tab-new";
    /** Constant for icon actions/view-fullscreen. */
    public static final String ACTIONS_VIEW_FULLSCREEN = "actions/view-fullscreen";
    /** Constant for icon actions/view-refresh. */
    public static final String ACTIONS_VIEW_REFRESH = "actions/view-refresh";
    /** Constant for icon actions/window-new. */
    public static final String ACTIONS_WINDOW_NEW = "actions/window-new";
    /** Constant for icon animations/process-working. */
    public static final String ANIMATIONS_PROCESS_WORKING = "animations/process-working";
    /** Constant for icon apps/accessories-calculator. */
    public static final String APPS_ACCESSORIES_CALCULATOR = "apps/accessories-calculator";
    /** Constant for icon apps/accessories-character-map. */
    public static final String APPS_ACCESSORIES_CHARACTER_MAP = "apps/accessories-character-map";
    /** Constant for icon apps/accessories-text-editor. */
    public static final String APPS_ACCESSORIES_TEXT_EDITOR = "apps/accessories-text-editor";
    /** Constant for icon apps/help-browser. */
    public static final String APPS_HELP_BROWSER = "apps/help-browser";
    /** Constant for icon apps/internet-group-chat. */
    public static final String APPS_INTERNET_GROUP_CHAT = "apps/internet-group-chat";
    /** Constant for icon apps/internet-mail. */
    public static final String APPS_INTERNET_MAIL = "apps/internet-mail";
    /** Constant for icon apps/internet-news-reader. */
    public static final String APPS_INTERNET_NEWS_READER = "apps/internet-news-reader";
    /** Constant for icon apps/internet-web-browser. */
    public static final String APPS_INTERNET_WEB_BROWSER = "apps/internet-web-browser";
    /** Constant for icon apps/office-calendar. */
    public static final String APPS_OFFICE_CALENDAR = "apps/office-calendar";
    /** Constant for icon apps/preferences-desktop-accessibility. */
    public static final String APPS_PREFERENCES_DESKTOP_ACCESSIBILITY = "apps/preferences-desktop-accessibility";
    /** Constant for icon apps/preferences-desktop-assistive-technology. */
    public static final String APPS_PREFERENCES_DESKTOP_ASSISTIVE_TECHNOLOGY = "apps/preferences-desktop-assistive-technology";
    /** Constant for icon apps/preferences-desktop-font. */
    public static final String APPS_PREFERENCES_DESKTOP_FONT = "apps/preferences-desktop-font";
    /** Constant for icon apps/preferences-desktop-keyboard-shortcuts. */
    public static final String APPS_PREFERENCES_DESKTOP_KEYBOARD_SHORTCUTS = "apps/preferences-desktop-keyboard-shortcuts";
    /** Constant for icon apps/preferences-desktop-locale. */
    public static final String APPS_PREFERENCES_DESKTOP_LOCALE = "apps/preferences-desktop-locale";
    /** Constant for icon apps/preferences-desktop-multimedia. */
    public static final String APPS_PREFERENCES_DESKTOP_MULTIMEDIA = "apps/preferences-desktop-multimedia";
    /** Constant for icon apps/preferences-desktop-remote-desktop. */
    public static final String APPS_PREFERENCES_DESKTOP_REMOTE_DESKTOP = "apps/preferences-desktop-remote-desktop";
    /** Constant for icon apps/preferences-desktop-screensaver. */
    public static final String APPS_PREFERENCES_DESKTOP_SCREENSAVER = "apps/preferences-desktop-screensaver";
    /** Constant for icon apps/preferences-desktop-theme. */
    public static final String APPS_PREFERENCES_DESKTOP_THEME = "apps/preferences-desktop-theme";
    /** Constant for icon apps/preferences-desktop-wallpaper. */
    public static final String APPS_PREFERENCES_DESKTOP_WALLPAPER = "apps/preferences-desktop-wallpaper";
    /** Constant for icon apps/preferences-system-network-proxy. */
    public static final String APPS_PREFERENCES_SYSTEM_NETWORK_PROXY = "apps/preferences-system-network-proxy";
    /** Constant for icon apps/preferences-system-session. */
    public static final String APPS_PREFERENCES_SYSTEM_SESSION = "apps/preferences-system-session";
    /** Constant for icon apps/preferences-system-windows. */
    public static final String APPS_PREFERENCES_SYSTEM_WINDOWS = "apps/preferences-system-windows";
    /** Constant for icon apps/system-file-manager. */
    public static final String APPS_SYSTEM_FILE_MANAGER = "apps/system-file-manager";
    /** Constant for icon apps/system-installer. */
    public static final String APPS_SYSTEM_INSTALLER = "apps/system-installer";
    /** Constant for icon apps/system-software-update. */
    public static final String APPS_SYSTEM_SOFTWARE_UPDATE = "apps/system-software-update";
    /** Constant for icon apps/system-users. */
    public static final String APPS_SYSTEM_USERS = "apps/system-users";
    /** Constant for icon apps/utilities-system-monitor. */
    public static final String APPS_UTILITIES_SYSTEM_MONITOR = "apps/utilities-system-monitor";
    /** Constant for icon apps/utilities-terminal. */
    public static final String APPS_UTILITIES_TERMINAL = "apps/utilities-terminal";
    /** Constant for icon categories/applications-accessories. */
    public static final String CATEGORIES_APPLICATIONS_ACCESSORIES = "categories/applications-accessories";
    /** Constant for icon categories/applications-development. */
    public static final String CATEGORIES_APPLICATIONS_DEVELOPMENT = "categories/applications-development";
    /** Constant for icon categories/applications-games. */
    public static final String CATEGORIES_APPLICATIONS_GAMES = "categories/applications-games";
    /** Constant for icon categories/applications-graphics. */
    public static final String CATEGORIES_APPLICATIONS_GRAPHICS = "categories/applications-graphics";
    /** Constant for icon categories/applications-internet. */
    public static final String CATEGORIES_APPLICATIONS_INTERNET = "categories/applications-internet";
    /** Constant for icon categories/applications-multimedia. */
    public static final String CATEGORIES_APPLICATIONS_MULTIMEDIA = "categories/applications-multimedia";
    /** Constant for icon categories/applications-office. */
    public static final String CATEGORIES_APPLICATIONS_OFFICE = "categories/applications-office";
    /** Constant for icon categories/applications-other. */
    public static final String CATEGORIES_APPLICATIONS_OTHER = "categories/applications-other";
    /** Constant for icon categories/applications-system. */
    public static final String CATEGORIES_APPLICATIONS_SYSTEM = "categories/applications-system";
    /** Constant for icon categories/preferences-desktop. */
    public static final String CATEGORIES_PREFERENCES_DESKTOP = "categories/preferences-desktop";
    /** Constant for icon categories/preferences-desktop-peripherals. */
    public static final String CATEGORIES_PREFERENCES_DESKTOP_PERIPHERALS = "categories/preferences-desktop-peripherals";
    /** Constant for icon categories/preferences-system. */
    public static final String CATEGORIES_PREFERENCES_SYSTEM = "categories/preferences-system";
    /** Constant for icon devices/audio-card. */
    public static final String DEVICES_AUDIO_CARD = "devices/audio-card";
    /** Constant for icon devices/audio-input-microphone. */
    public static final String DEVICES_AUDIO_INPUT_MICROPHONE = "devices/audio-input-microphone";
    /** Constant for icon devices/battery. */
    public static final String DEVICES_BATTERY = "devices/battery";
    /** Constant for icon devices/camera-photo. */
    public static final String DEVICES_CAMERA_PHOTO = "devices/camera-photo";
    /** Constant for icon devices/camera-video. */
    public static final String DEVICES_CAMERA_VIDEO = "devices/camera-video";
    /** Constant for icon devices/computer. */
    public static final String DEVICES_COMPUTER = "devices/computer";
    /** Constant for icon devices/drive-harddisk. */
    public static final String DEVICES_DRIVE_HARDDISK = "devices/drive-harddisk";
    /** Constant for icon devices/drive-optical. */
    public static final String DEVICES_DRIVE_OPTICAL = "devices/drive-optical";
    /** Constant for icon devices/drive-removable-media. */
    public static final String DEVICES_DRIVE_REMOVABLE_MEDIA = "devices/drive-removable-media";
    /** Constant for icon devices/input-gaming. */
    public static final String DEVICES_INPUT_GAMING = "devices/input-gaming";
    /** Constant for icon devices/input-keyboard. */
    public static final String DEVICES_INPUT_KEYBOARD = "devices/input-keyboard";
    /** Constant for icon devices/input-mouse. */
    public static final String DEVICES_INPUT_MOUSE = "devices/input-mouse";
    /** Constant for icon devices/media-flash. */
    public static final String DEVICES_MEDIA_FLASH = "devices/media-flash";
    /** Constant for icon devices/media-floppy. */
    public static final String DEVICES_MEDIA_FLOPPY = "devices/media-floppy";
    /** Constant for icon devices/media-optical. */
    public static final String DEVICES_MEDIA_OPTICAL = "devices/media-optical";
    /** Constant for icon devices/multimedia-player. */
    public static final String DEVICES_MULTIMEDIA_PLAYER = "devices/multimedia-player";
    /** Constant for icon devices/network-wired. */
    public static final String DEVICES_NETWORK_WIRED = "devices/network-wired";
    /** Constant for icon devices/network-wireless. */
    public static final String DEVICES_NETWORK_WIRELESS = "devices/network-wireless";
    /** Constant for icon devices/printer. */
    public static final String DEVICES_PRINTER = "devices/printer";
    /** Constant for icon devices/video-display. */
    public static final String DEVICES_VIDEO_DISPLAY = "devices/video-display";
    /** Constant for icon emblems/emblem-favorite. */
    public static final String EMBLEMS_EMBLEM_FAVORITE = "emblems/emblem-favorite";
    /** Constant for icon emblems/emblem-important. */
    public static final String EMBLEMS_EMBLEM_IMPORTANT = "emblems/emblem-important";
    /** Constant for icon emblems/emblem-photos. */
    public static final String EMBLEMS_EMBLEM_PHOTOS = "emblems/emblem-photos";
    /** Constant for icon emblems/emblem-readonly. */
    public static final String EMBLEMS_EMBLEM_READONLY = "emblems/emblem-readonly";
    /** Constant for icon emblems/emblem-symbolic-link. */
    public static final String EMBLEMS_EMBLEM_SYMBOLIC_LINK = "emblems/emblem-symbolic-link";
    /** Constant for icon emblems/emblem-system. */
    public static final String EMBLEMS_EMBLEM_SYSTEM = "emblems/emblem-system";
    /** Constant for icon emblems/emblem-unreadable. */
    public static final String EMBLEMS_EMBLEM_UNREADABLE = "emblems/emblem-unreadable";
    /** Constant for icon emotes/face-angel. */
    public static final String EMOTES_FACE_ANGEL = "emotes/face-angel";
    /** Constant for icon emotes/face-crying. */
    public static final String EMOTES_FACE_CRYING = "emotes/face-crying";
    /** Constant for icon emotes/face-devilish. */
    public static final String EMOTES_FACE_DEVILISH = "emotes/face-devilish";
    /** Constant for icon emotes/face-glasses. */
    public static final String EMOTES_FACE_GLASSES = "emotes/face-glasses";
    /** Constant for icon emotes/face-grin. */
    public static final String EMOTES_FACE_GRIN = "emotes/face-grin";
    /** Constant for icon emotes/face-kiss. */
    public static final String EMOTES_FACE_KISS = "emotes/face-kiss";
    /** Constant for icon emotes/face-monkey. */
    public static final String EMOTES_FACE_MONKEY = "emotes/face-monkey";
    /** Constant for icon emotes/face-plain. */
    public static final String EMOTES_FACE_PLAIN = "emotes/face-plain";
    /** Constant for icon emotes/face-sad. */
    public static final String EMOTES_FACE_SAD = "emotes/face-sad";
    /** Constant for icon emotes/face-smile. */
    public static final String EMOTES_FACE_SMILE = "emotes/face-smile";
    /** Constant for icon emotes/face-smile-big. */
    public static final String EMOTES_FACE_SMILE_BIG = "emotes/face-smile-big";
    /** Constant for icon emotes/face-surprise. */
    public static final String EMOTES_FACE_SURPRISE = "emotes/face-surprise";
    /** Constant for icon emotes/face-wink. */
    public static final String EMOTES_FACE_WINK = "emotes/face-wink";
    /** Constant for icon mimetypes/application-certificate. */
    public static final String MIMETYPES_APPLICATION_CERTIFICATE = "mimetypes/application-certificate";
    /** Constant for icon mimetypes/application-x-executable. */
    public static final String MIMETYPES_APPLICATION_X_EXECUTABLE = "mimetypes/application-x-executable";
    /** Constant for icon mimetypes/audio-x-generic. */
    public static final String MIMETYPES_AUDIO_X_GENERIC = "mimetypes/audio-x-generic";
    /** Constant for icon mimetypes/font-x-generic. */
    public static final String MIMETYPES_FONT_X_GENERIC = "mimetypes/font-x-generic";
    /** Constant for icon mimetypes/image-x-generic. */
    public static final String MIMETYPES_IMAGE_X_GENERIC = "mimetypes/image-x-generic";
    /** Constant for icon mimetypes/package-x-generic. */
    public static final String MIMETYPES_PACKAGE_X_GENERIC = "mimetypes/package-x-generic";
    /** Constant for icon mimetypes/text-html. */
    public static final String MIMETYPES_TEXT_HTML = "mimetypes/text-html";
    /** Constant for icon mimetypes/text-x-generic. */
    public static final String MIMETYPES_TEXT_X_GENERIC = "mimetypes/text-x-generic";
    /** Constant for icon mimetypes/text-x-generic-template. */
    public static final String MIMETYPES_TEXT_X_GENERIC_TEMPLATE = "mimetypes/text-x-generic-template";
    /** Constant for icon mimetypes/text-x-script. */
    public static final String MIMETYPES_TEXT_X_SCRIPT = "mimetypes/text-x-script";
    /** Constant for icon mimetypes/video-x-generic. */
    public static final String MIMETYPES_VIDEO_X_GENERIC = "mimetypes/video-x-generic";
    /** Constant for icon mimetypes/x-office-address-book. */
    public static final String MIMETYPES_X_OFFICE_ADDRESS_BOOK = "mimetypes/x-office-address-book";
    /** Constant for icon mimetypes/x-office-calendar. */
    public static final String MIMETYPES_X_OFFICE_CALENDAR = "mimetypes/x-office-calendar";
    /** Constant for icon mimetypes/x-office-document. */
    public static final String MIMETYPES_X_OFFICE_DOCUMENT = "mimetypes/x-office-document";
    /** Constant for icon mimetypes/x-office-document-template. */
    public static final String MIMETYPES_X_OFFICE_DOCUMENT_TEMPLATE = "mimetypes/x-office-document-template";
    /** Constant for icon mimetypes/x-office-drawing. */
    public static final String MIMETYPES_X_OFFICE_DRAWING = "mimetypes/x-office-drawing";
    /** Constant for icon mimetypes/x-office-drawing-template. */
    public static final String MIMETYPES_X_OFFICE_DRAWING_TEMPLATE = "mimetypes/x-office-drawing-template";
    /** Constant for icon mimetypes/x-office-presentation. */
    public static final String MIMETYPES_X_OFFICE_PRESENTATION = "mimetypes/x-office-presentation";
    /** Constant for icon mimetypes/x-office-presentation-template. */
    public static final String MIMETYPES_X_OFFICE_PRESENTATION_TEMPLATE = "mimetypes/x-office-presentation-template";
    /** Constant for icon mimetypes/x-office-spreadsheet. */
    public static final String MIMETYPES_X_OFFICE_SPREADSHEET = "mimetypes/x-office-spreadsheet";
    /** Constant for icon mimetypes/x-office-spreadsheet-template. */
    public static final String MIMETYPES_X_OFFICE_SPREADSHEET_TEMPLATE = "mimetypes/x-office-spreadsheet-template";
    /** Constant for icon places/folder. */
    public static final String PLACES_FOLDER = "places/folder";
    /** Constant for icon places/folder-remote. */
    public static final String PLACES_FOLDER_REMOTE = "places/folder-remote";
    /** Constant for icon places/folder-saved-search. */
    public static final String PLACES_FOLDER_SAVED_SEARCH = "places/folder-saved-search";
    /** Constant for icon places/network-server. */
    public static final String PLACES_NETWORK_SERVER = "places/network-server";
    /** Constant for icon places/network-workgroup. */
    public static final String PLACES_NETWORK_WORKGROUP = "places/network-workgroup";
    /** Constant for icon places/start-here. */
    public static final String PLACES_START_HERE = "places/start-here";
    /** Constant for icon places/user-desktop. */
    public static final String PLACES_USER_DESKTOP = "places/user-desktop";
    /** Constant for icon places/user-home. */
    public static final String PLACES_USER_HOME = "places/user-home";
    /** Constant for icon places/user-trash. */
    public static final String PLACES_USER_TRASH = "places/user-trash";
    /** Constant for icon status/audio-volume-high. */
    public static final String STATUS_AUDIO_VOLUME_HIGH = "status/audio-volume-high";
    /** Constant for icon status/audio-volume-low. */
    public static final String STATUS_AUDIO_VOLUME_LOW = "status/audio-volume-low";
    /** Constant for icon status/audio-volume-medium. */
    public static final String STATUS_AUDIO_VOLUME_MEDIUM = "status/audio-volume-medium";
    /** Constant for icon status/audio-volume-muted. */
    public static final String STATUS_AUDIO_VOLUME_MUTED = "status/audio-volume-muted";
    /** Constant for icon status/battery-caution. */
    public static final String STATUS_BATTERY_CAUTION = "status/battery-caution";
    /** Constant for icon status/dialog-error. */
    public static final String STATUS_DIALOG_ERROR = "status/dialog-error";
    /** Constant for icon status/dialog-information. */
    public static final String STATUS_DIALOG_INFORMATION = "status/dialog-information";
    /** Constant for icon status/dialog-warning. */
    public static final String STATUS_DIALOG_WARNING = "status/dialog-warning";
    /** Constant for icon status/folder-drag-accept. */
    public static final String STATUS_FOLDER_DRAG_ACCEPT = "status/folder-drag-accept";
    /** Constant for icon status/folder-open. */
    public static final String STATUS_FOLDER_OPEN = "status/folder-open";
    /** Constant for icon status/folder-visiting. */
    public static final String STATUS_FOLDER_VISITING = "status/folder-visiting";
    /** Constant for icon status/image-loading. */
    public static final String STATUS_IMAGE_LOADING = "status/image-loading";
    /** Constant for icon status/image-missing. */
    public static final String STATUS_IMAGE_MISSING = "status/image-missing";
    /** Constant for icon status/mail-attachment. */
    public static final String STATUS_MAIL_ATTACHMENT = "status/mail-attachment";
    /** Constant for icon status/network-error. */
    public static final String STATUS_NETWORK_ERROR = "status/network-error";
    /** Constant for icon status/network-idle. */
    public static final String STATUS_NETWORK_IDLE = "status/network-idle";
    /** Constant for icon status/network-offline. */
    public static final String STATUS_NETWORK_OFFLINE = "status/network-offline";
    /** Constant for icon status/network-receive. */
    public static final String STATUS_NETWORK_RECEIVE = "status/network-receive";
    /** Constant for icon status/network-transmit. */
    public static final String STATUS_NETWORK_TRANSMIT = "status/network-transmit";
    /** Constant for icon status/network-transmit-receive. */
    public static final String STATUS_NETWORK_TRANSMIT_RECEIVE = "status/network-transmit-receive";
    /** Constant for icon status/network-wireless-encrypted. */
    public static final String STATUS_NETWORK_WIRELESS_ENCRYPTED = "status/network-wireless-encrypted";
    /** Constant for icon status/printer-error. */
    public static final String STATUS_PRINTER_ERROR = "status/printer-error";
    /** Constant for icon status/software-update-available. */
    public static final String STATUS_SOFTWARE_UPDATE_AVAILABLE = "status/software-update-available";
    /** Constant for icon status/software-update-urgent. */
    public static final String STATUS_SOFTWARE_UPDATE_URGENT = "status/software-update-urgent";
    /** Constant for icon status/user-trash-full. */
    public static final String STATUS_USER_TRASH_FULL = "status/user-trash-full";
    /** Constant for icon status/weather-clear. */
    public static final String STATUS_WEATHER_CLEAR = "status/weather-clear";
    /** Constant for icon status/weather-clear-night. */
    public static final String STATUS_WEATHER_CLEAR_NIGHT = "status/weather-clear-night";
    /** Constant for icon status/weather-few-clouds. */
    public static final String STATUS_WEATHER_FEW_CLOUDS = "status/weather-few-clouds";
    /** Constant for icon status/weather-few-clouds-night. */
    public static final String STATUS_WEATHER_FEW_CLOUDS_NIGHT = "status/weather-few-clouds-night";
    /** Constant for icon status/weather-overcast. */
    public static final String STATUS_WEATHER_OVERCAST = "status/weather-overcast";
    /** Constant for icon status/weather-severe-alert. */
    public static final String STATUS_WEATHER_SEVERE_ALERT = "status/weather-severe-alert";
    /** Constant for icon status/weather-showers. */
    public static final String STATUS_WEATHER_SHOWERS = "status/weather-showers";
    /** Constant for icon status/weather-showers-scattered. */
    public static final String STATUS_WEATHER_SHOWERS_SCATTERED = "status/weather-showers-scattered";
    /** Constant for icon status/weather-snow. */
    public static final String STATUS_WEATHER_SNOW = "status/weather-snow";
    /** Constant for icon status/weather-storm. */
    public static final String STATUS_WEATHER_STORM = "status/weather-storm";

    private IconConstants() {
        // nothing to do.
    }

}
