package name.berger.max.mil;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Icon Finder helper. This class allows to specify icons by their group, name,
 * and size. It will then load the matching icon from the local file system,
 * falling back to defaults if necessary.
 * 
 * @version $Date$
 */
public class IconFinder {

    private static final Log LOGGER = LogFactory.getLog(IconFinder.class);
    private final List<URL> iconPath = new LinkedList<URL>();

    /**
     * Default constructor. Preloads paths.
     */
    public IconFinder() {
        final URL url = ClassLoader.getSystemResource("tango/");
        if (url != null) {
            this.prefixPath(url);
        }

        // prefixPath(new URI("jar:"))
        this.prefixUnixPaths("hicolor");
        final String s = new GnomeThemeDetector().getTheme();
        if (s != null) {
            this.prefixUnixPaths(s);
        }
    }

    private void prefixUnixPaths(final String string) {
        this.prefixPath("/usr/share/icons/" + string);
        final String home = System.getProperty("user.home");
        this.prefixPath(home + "/.local/share/icons/" + string);

    }

    private void prefixPath(final URL url) {
        this.iconPath.add(0, url);
    }

    private void prefixPath(final String localPath) {
        try {
            this.prefixPath(new URL("file:///" + localPath + "/"));
        } catch (final MalformedURLException e) {
            // This happens on windows with user.home, but we don't care.
            IconFinder.LOGGER.debug(e.toString(), e);
        }
    }

    /**
     * Retrieve an icon URL for the given icon.
     * 
     * @param size
     *            size of the icon.
     * @param groupAndName
     *            Name of the Group and the icon in the format "group/icon".
     * @return URL of the icon or null if the icon could not be found.
     */
    public URL getIconUrl(final int size, final String groupAndName) {
        final StringBuilder b = new StringBuilder();
        b.append(size).append('x').append(size).append('/');
        b.append(groupAndName).append('.').append("png");
        final String relPath = b.toString();
        for (final URL url : this.iconPath) {
            try {
                final URL completeUrl = new URL(url + relPath);
                try {
                    final InputStream is = completeUrl.openStream();
                    is.close();
                    return completeUrl;
                } catch (final IOException e) {
                    IconFinder.LOGGER
                            .debug("Not accessible: " + completeUrl, e);
                }
            } catch (final MalformedURLException e) {
                IconFinder.LOGGER.warn("Illegal URL: ", e);
            }
        }
        return null;
    }

    /**
     * Retrieve an icon for the given names.
     * <p>
     * This method will always return a valid icon, falling back to default
     * icons if the requested icon is not available.
     * 
     * @param size
     *            size of the icon.
     * @param groupAndName
     *            Name of the Group and the icon in the format "group/icon".
     * @return a valid icon with the given contents.
     */
    public Icon getIcon(final int size, final String groupAndName) {
        URL url = this.getIconUrl(size, groupAndName);
        if (url == null) {
            url = this.getIconUrl(size, IconConstants.STATUS_IMAGE_MISSING);
        }
        if (url == null) {
            return new ImageIcon(new BufferedImage(size, size,
                    BufferedImage.TYPE_4BYTE_ABGR));
        } else {
            return new ImageIcon(url);
        }
    }
}
