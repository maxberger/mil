package name.berger.max.mil;

import java.net.URL;

import javax.swing.Icon;

import org.testng.Assert;

/**
 * Unit test for simple Mil.
 * 
 * @version $Date$
 */
public class IconFinderTest {

    private static final int TEST_ICON_SIZE = 22;

    private static final String BAD_ICON = "actions/bla-bla";

    /**
     * Default constructor.
     */
    public IconFinderTest() {
        // Nothing to do.
    }

    /**
     * Assert that valid urls or null is returned.
     */
    @org.testng.annotations.Test
    public void testUrl() {
        final IconFinder f = new IconFinder();
        final URL url = f.getIconUrl(IconFinderTest.TEST_ICON_SIZE,
                IconConstants.ACTIONS_EDIT_CUT);
        Assert.assertNotNull(url);
        Assert.assertNull(f.getIconUrl(IconFinderTest.TEST_ICON_SIZE,
                IconFinderTest.BAD_ICON));
        System.out.println("Debug: URL = " + url);
    }

    /**
     * Assert that icons are returned.
     */
    @org.testng.annotations.Test
    public void testIcon() {
        final IconFinder f = new IconFinder();
        final Icon icon = f.getIcon(IconFinderTest.TEST_ICON_SIZE,
                IconConstants.ACTIONS_EDIT_CUT);
        Assert.assertNotNull(icon);
        Assert
                .assertEquals(icon.getIconHeight(),
                        IconFinderTest.TEST_ICON_SIZE);
        Assert.assertEquals(icon.getIconWidth(), IconFinderTest.TEST_ICON_SIZE);
        final Icon icon2 = f.getIcon(IconFinderTest.TEST_ICON_SIZE,
                IconFinderTest.BAD_ICON);
        Assert.assertNotNull(icon2);
        Assert.assertEquals(icon2.getIconHeight(),
                IconFinderTest.TEST_ICON_SIZE);
        Assert
                .assertEquals(icon2.getIconWidth(),
                        IconFinderTest.TEST_ICON_SIZE);
    }
}
